let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}
const {name,birthday,age,classes} = student1;

function introduce(student){

	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old. I study the following courses ${classes}`);
}

class Dog {
	constructor(name,breed,dogAge){
		this.name = name
		this.breed = breed
		this.dogAge = dogAge*7
	}
}
let dog1 = new Dog ("Luffy","Bulldog",3)
let dog2 = new Dog ("Zoro","Labrador",2)

console.log(dog1)
console.log(dog2)

const getCube = (num) => Math.pow(num,3)
let cube = getCube(3);
console.log(cube)



let numArr = [15,16,32,21,21,2]
numArr.forEach((num) =>console.log(num))
console.log()

let numSquared = numArr.map ((num) =>num ** 2)
console.log(numSquared);
